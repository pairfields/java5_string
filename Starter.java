package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/*
1.  Введите любую строку. Замените в строке каждое второе вхождение «object-oriented programming» (не учитываем регистр символов) на «OOP»
    Например, строка "Object-oriented programming is a programming language model organized around objects rather than "actions" and data rather than logic. Object-oriented programming blabla. Object-oriented programming bla." должна быть преобразована в "Object-oriented programming is a programming language model organized around objects rather than "actions" and data rather than logic. OOP blabla. Object-oriented programming bla."
2.  Найти слово, в котором число различных символов минимально
    Слово может содержать буквы и цифры
    Если таких слов несколько, найти первое из них
    Например, в строке "fffff ab f 1234 jkjk" найденное слово должно быть "fffff"
3.  Найти количество слов, содержащих только символы латинского алфавита
4.  Найти слова палиндромы
 */
public class Starter {


    public static void main(String[] args) {
        //1
        Scanner scanner = new Scanner(System.in);
        String userString = scanner.nextLine();

        final String lowerCaseString = userString.toLowerCase(Locale.ROOT);
        final String phrase = "object-oriented programming";
        StringBuilder resultString  = new StringBuilder();
        int subIdx = 0;
        ArrayList<Integer> arrayIdx = new ArrayList<>();
        ArrayList<String> arrayString = new ArrayList<>();

        // Находим индексы каждого вхождения фразы
        while (subIdx != -1) {
            subIdx = lowerCaseString.indexOf(phrase, subIdx);
            arrayIdx.add(subIdx);
            subIdx = subIdx == -1 ? subIdx : subIdx + 1;
        }
        // устанавливаем в массиве индексов последним элементом длинну массива
        arrayIdx.set(arrayIdx.size() - 1, userString.length());

        for (int i = 0; i<arrayIdx.size()-1; i++){
            // с помощью найденных индексов разбиваем строку,введенную пользователем, на массив строк, так чтобы каждая строка начиналась с искомой фразы
            arrayString.add(userString.substring(arrayIdx.get(i), arrayIdx.get(i + 1)));
            if ((i+1)%2 == 0){
                //  и заменяем для всех четных элементов массива начало строки на ООР
                    arrayString.set(i, arrayString.get(i).replace(arrayString.get(i).substring(0 , phrase.length()),"OOP"));
                }
            resultString.append(arrayString.get(i)); // собираем итоговую строку
        }
        // в итоговое строке каждое второе вхождение Object-oriented programming вне зависимости от регистра будет заменено на ООР, а остальные вхождения останутся неизмеными
        System.out.println(resultString);

        //2
        int maxLetters = userString.length(); // длинна слова не может быть больше длинны строки
        String minWord = null;
        String[] words = userString.split(" "); // разбиваем строку по пробелам
        for (String word : words) {
            String[] letters = word.split(""); // разбиваем каждое слово на буквы
            ArrayList<String> uniqueLetters = new ArrayList<>(); // в этмо массиве храним уникальные буквы, на каждой итерации он обновляется
            for (String letter : letters) {
                if (!uniqueLetters.contains(letter)) { // если буквы нет в массиве, то запсиываем её
                    uniqueLetters.add(letter);
                }
            }
            if (uniqueLetters.size() < maxLetters) { // если длинна массива уникальных букв строго меньше максимального кол-ва символов,
                // то обновляем значения, таким образом добивается требовании о записи самого первого слова
                maxLetters = uniqueLetters.size();
                minWord = word;
            }
        }
        System.out.println(minWord);
        //3
        int counter = 0;
        Pattern pattern = Pattern.compile("[a-zA-Z]+"); // регулярное выражение закрывающее все слова латинского алфавита
        Matcher matcher = pattern.matcher(userString);
        while (matcher.find()) { // на каждой итерации ищем в строке слово, в случае успеха прибавляем к счетчику 1
            counter++;
        }
        System.out.println(counter);

        //4
        for(String word: words){ // бежим по массиву слов
            word = word.intern(); // записываем очередное слово в стрингпул
            String drow = new StringBuilder(word).reverse().toString(); // получаем строку в обратном порядке
            drow = drow.intern(); // записываем полученную строку в стрингпул, в случае если такая строка там уже есть, это будет значить, что мы нашли полиндром
            if (word == drow){
                System.out.println(word);
            }
        }

    }
}

